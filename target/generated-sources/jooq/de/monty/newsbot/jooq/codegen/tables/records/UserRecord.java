/*
 * This file is generated by jOOQ.
 */
package de.monty.newsbot.jooq.codegen.tables.records;


import de.monty.newsbot.jooq.codegen.tables.User;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.9"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserRecord extends UpdatableRecordImpl<UserRecord> implements Record6<Integer, String, String, String, Boolean, Boolean> {

    private static final long serialVersionUID = 546220357;

    /**
     * Setter for <code>User.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>User.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>User.Usertag</code>.
     */
    public void setUsertag(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>User.Usertag</code>.
     */
    public String getUsertag() {
        return (String) get(1);
    }

    /**
     * Setter for <code>User.Userid</code>.
     */
    public void setUserid(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>User.Userid</code>.
     */
    public String getUserid() {
        return (String) get(2);
    }

    /**
     * Setter for <code>User.Regtype</code>.
     */
    public void setRegtype(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>User.Regtype</code>.
     */
    public String getRegtype() {
        return (String) get(3);
    }

    /**
     * Setter for <code>User.Badge</code>.
     */
    public void setBadge(Boolean value) {
        set(4, value);
    }

    /**
     * Getter for <code>User.Badge</code>.
     */
    public Boolean getBadge() {
        return (Boolean) get(4);
    }

    /**
     * Setter for <code>User.isAdmin</code>.
     */
    public void setIsadmin(Boolean value) {
        set(5, value);
    }

    /**
     * Getter for <code>User.isAdmin</code>.
     */
    public Boolean getIsadmin() {
        return (Boolean) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, Boolean, Boolean> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, String, String, String, Boolean, Boolean> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return User.USER.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return User.USER.USERTAG;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return User.USER.USERID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return User.USER.REGTYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field5() {
        return User.USER.BADGE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Boolean> field6() {
        return User.USER.ISADMIN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getUsertag();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getUserid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getRegtype();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean component5() {
        return getBadge();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean component6() {
        return getIsadmin();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getUsertag();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getUserid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getRegtype();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value5() {
        return getBadge();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean value6() {
        return getIsadmin();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value2(String value) {
        setUsertag(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value3(String value) {
        setUserid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value4(String value) {
        setRegtype(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value5(Boolean value) {
        setBadge(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value6(Boolean value) {
        setIsadmin(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord values(Integer value1, String value2, String value3, String value4, Boolean value5, Boolean value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UserRecord
     */
    public UserRecord() {
        super(User.USER);
    }

    /**
     * Create a detached, initialised UserRecord
     */
    public UserRecord(Integer id, String usertag, String userid, String regtype, Boolean badge, Boolean isadmin) {
        super(User.USER);

        set(0, id);
        set(1, usertag);
        set(2, userid);
        set(3, regtype);
        set(4, badge);
        set(5, isadmin);
    }
}
