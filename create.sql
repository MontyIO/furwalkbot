CREATE TABLE IF NOT EXISTS User (
    id integer PRIMARY KEY autoincrement,
    Usertag text NOT NULL,
    Userid long,
    Regtype text,
    Badge boolean,
    isAdmin boolean,
    Status text
);
