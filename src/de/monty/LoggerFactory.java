package de.monty;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

final class LoggerFactory {

    private static final Logger LOG;

    static {
        // configure Logger
        LOG = Logger.getLogger(Main.class.getSimpleName());
        try {
            FileHandler fh = new FileHandler("log.txt", true);
            fh.setFormatter(new SimpleFormatter());
            LOG.addHandler(fh);
        } catch (SecurityException | IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private LoggerFactory() {
    }

    public static Logger getLoggerInstance() {
        return LOG;
    }
}
