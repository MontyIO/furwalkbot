package de.monty;

public enum Status {
    NONE,
    REGTYPE,
    BADGE,
    BADGE_2,
    FOOD,
    EDIT_REGTYPE,
    EDIT_BADGE
}
