package de.monty.Methods;

public class TextFactory {

    Database db = new Database();

    public String buildRegInfo (int uid) {

        String out;
        String regtype = db.getRegtype(uid);
        String badge;

        if (db.getBadge(uid)) {
            badge = "-Badge(3€): *Bestellt*" +"\n";
        } else {
            badge = "-Badge(3€): *Nicht Bestellt*" +"\n";
        }

        out = "*Dies sind ihre derzeitigen Anmeldedaten:* "
                +"\n"
                +"\n"
                +"Sie sind angemeldet als: "
                + "*" +regtype +"*"
                +"\n\n"
                +"Zusätzliche informationen:"
                +"\n"
                + badge;

        return out;
    }

    public String buildRegtypeUserList () {

        String fursuiter = String.join("\n", db.getUserByRegtype("Fursuiter"));
        String Spotter = String.join("\n", db.getUserByRegtype("Spotter"));
        String medienfurs = String.join("\n", db.getUserByRegtype("Medienfur"));
        String begleiter = String.join("\n", db.getUserByRegtype("Begleiter"));

        int suitCount = db.getUserByRegtype("Fursuiter").size();
        int spotCount = db.getUserByRegtype("Spotter").size();
        int mediCount = db.getUserByRegtype("Medienfur").size();
        int beglCount = db.getUserByRegtype("Begleiter").size();


        String out = "Angemeldete Nutzer kategorisiert nach Registrationstypen:"
                + "\n"
                + "\n"
                + suitCount +" Fursuiter:"
                + "\n"
                + "\n"
                + fursuiter
                + "\n"
                + "\n"
                + spotCount +" Spotter:"
                + "\n"
                + "\n"
                + Spotter
                + "\n"
                + "\n"
                + mediCount +" MedienFurs:"
                + "\n"
                + "\n"
                + medienfurs
                + "\n"
                + "\n"
                + beglCount +" Begleiter:"
                + "\n"
                + "\n"
                + begleiter
                + "\n"
                + "\n"
                +getRegCount()
                + "\n"
                + "\n"
                +"Kehren sie mit /menu in das Menü zurück";

        return out;
    }

    public String buildBadgeList () {

        String out = "Diese Nutzer haben zusätzlich eine Badge bestellt:"
                + "\n"
                + "\n"
                + String.join("\n", db.getBadgeOrders())
                + "\n"
                + "\n"
                +"Kehren sie mit /admin in das Menü zurück";

        return out;
    }

    public String infoText () {
        String out = "*Herbstwalk im Britzer Garten am 12.10.*\n" +
                "\n" +
                "Der Herbst naht mit schnellen Schritten Buntes Laub fällt von den Bäumen. Es wird Zeit für einen bunten Walk im Britzer Garten!\n" +
                "\n" +
                "Diesmal geht es hoch hinaus auf einen Berg und dann zur größten Sonnenuhr Europas™. Natürlich gibt es wieder die Umkleide an der Hauptbühne. Da wir diesmal ohne Veranstaltung da sind, gibt es leider keine Freikarten. Die Eintrittspreise für den Britzer Garten sind zwischen 3 € und 1,50 € ermäßigt. \n" +
                "\n" +
                "Am selben Tag ist dann wieder auch der Stammtisch in Spandau. Entsprechend, wer Lust hat, wir treffen uns hinterher dort. \n" +
                "\n" +
                "Wie üblich biete ich ein Badge an, mit Band. Diesmal für 3 €. Anmeldung für Badge bis 07.10. - 16 Uhr. Über kleine Spenden freue ich mich. \n" +
                "\n" +
                "Denkt daran, dass der beste Eingang der Haupteingang ist. Mohriner Allee  152, 12349 Berlin.\n" +
                "\n" +
                "Zeitplan: \n" +
                "13 Uhr – Treffen am Haupteingang\n" +
                "13:30 Uhr – Treffen am Hintereingang der Festhalle\n" +
                "14 Uhr – Start des Suitwalk\n" +
                "16 Uhr – Ende Suitwalk\n" +
                "18 Uhr – Stammtisch Spandau";

        return out;
    }

    public String getRegCount() {
        int value = db.getRegistrationCount().size();


        String out = "Anzahl der Anmeldungen: " +value;

        return out;
    }
}
