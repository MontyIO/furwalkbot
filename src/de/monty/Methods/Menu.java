package de.monty.Methods;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class Menu {

    TextFactory tf = new TextFactory();

    public SendMessage unregStart(long cid) {

        SendMessage menu = InlineKeyboardBuilder.create(cid)
                .setText(tf.infoText())
                .row()
                .button ("Anmelden", "register")
                .endRow()
                .build();

        return menu;
    }

    public SendMessage regStart (int uid, long cid) {

        SendMessage menu = InlineKeyboardBuilder.create(cid)
                .setText(tf.infoText() + "\n" + "\n" + tf.buildRegInfo(uid) + "\n\n" +"*Hier können sie ihre Einstellungen ändern:*")
                .row()
                .button("Registrationskategorie ändern", "editRegtype")
                .endRow()
                .row()
                .button("Liste der Teilnehmer", "Reglist")
                .endRow()
                .row()
                .button("Badge-Bestellung ändern", "editBadge")
                .endRow()
                .row()
                .button("Abmelden", "deregister")
                .endRow()
                .build();

        return menu;
    }

    public SendMessage adminStart (long cid, String utg) {

        SendMessage menu = InlineKeyboardBuilder.create(cid)
                .setText(tf.infoText()+"\n \n" +tf.getRegCount() +"\n" +"\n" + "Willkommen @" +utg)
                .row()
                .button("Liste der Anmeldungen anzeigen", "Reglist")
                .endRow()
                .row()
                .button("Badge Bestellungen anzeigen", "Badgelist")
                .endRow()
                .build();

        return menu;
    }

    public SendMessage regtypeMenu(long cid) {
        SendMessage menu = InlineKeyboardBuilder.create(cid)
                .setText("Ich nehme teil als:")
                .row()
                .button("Fursuiter", "Fursuiter")
                .endRow()
                .row()
                .button("Spotter", "Spotter")
                .endRow()
                .row()
                .button("MedienFur", "Medienfur")
                .endRow()
                .row()
                .button("Begleiter", "Begleiter")
                .endRow()
                .build();

        return menu;
    }

    public SendMessage badgeMenu (long cid) {
        SendMessage menu = InlineKeyboardBuilder.create(cid)
                .setText("Ich möchte zusätzlich eine Badge für 2€ erwerben:")
                .row()
                .button("Ja", "badge")
                .button("Nein", "!badge")
                .endRow()
                .build();

        return menu;
    }
}
