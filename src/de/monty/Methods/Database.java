package de.monty.Methods;

import de.monty.Status;
import de.monty.jooq.codegen.Tables;
import de.monty.jooq.codegen.tables.User;
import de.monty.jooq.codegen.tables.records.UserRecord;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import javax.swing.event.ListDataEvent;
import java.util.List;

public final class Database {

    private static  final String dbConnectionUrl = "jdbc:sqlite:./Furwalk.db";
    private static User userTable = Tables.USER;

    private final DSLContext dsl;

   public Database() {dsl = DSL.using(dbConnectionUrl);}

    public void insertUser (String utg, int uid, String regtype, boolean badge, boolean isAdmin) {
        DSLContext dsl = DSL.using(dbConnectionUrl);

        dsl.insertInto(userTable)
                .set(userTable.USERTAG, "@" +utg)
                .set(userTable.USERID, uid)
                .set(userTable.REGTYPE, regtype)
                .set(userTable.BADGE, badge)
                .set(userTable.ISADMIN, isAdmin)
                .set(userTable.STATUS, Status.NONE)
                .execute();
    }

    public Status getStatus (int uid) {
        DSLContext dsl = DSL.using(dbConnectionUrl);

        return dsl.select(userTable.STATUS)
                .from(userTable)
                .where(userTable.USERID.eq(uid))
                .fetchOneInto(Status.class);

    }

    public boolean noUserExists() {
        DSLContext dsl = DSL.using(dbConnectionUrl);

        return dsl.selectOne()
                .from(userTable)
                .fetch()
                .isEmpty();
    }

    public void updateStatus (Status status, int uid) {
       dsl.update(userTable)
               .set(userTable.STATUS, status)
               .where(userTable.USERID.eq(uid))
               .execute();
    }

    public UserRecord getAdminStatus(int uid) {
        DSLContext dsl = DSL.using(dbConnectionUrl);

        return dsl.select(userTable.ISADMIN)
                 .from(userTable)
                 .where(userTable.USERID.eq(uid))
                 .fetchOneInto(UserRecord.class);
    }

    public UserRecord getUser (int uid) {
       return dsl.select()
               .from(userTable)
               .where(userTable.USERID.eq(uid))
               .fetchOneInto(UserRecord.class);
    }

    public void updateRegtype (String regtype, int uid) {
       DSLContext dsl = DSL.using(dbConnectionUrl);

       dsl.update(userTable)
               .set(userTable.REGTYPE, regtype)
               .where(userTable.USERID.eq(uid))
               .execute();
    }

    public void updateBadge (int uid, boolean value) {
        DSLContext dsl = DSL.using(dbConnectionUrl);

       dsl.update(userTable)
               .set(userTable.BADGE, value)
               .where(userTable.USERID.eq(uid))
               .execute();
    }

    public String getRegtype (int uid) {
        DSLContext dsl = DSL.using(dbConnectionUrl);

        return dsl.select(userTable.REGTYPE)
                .from(userTable)
                .where(userTable.USERID.eq(uid))
                .fetchOneInto(String.class);
    }

    public boolean getBadge(int uid) {
       DSLContext dsl = DSL.using(dbConnectionUrl);

       return dsl.select(userTable.BADGE)
               .from(userTable)
               .where(userTable.USERID.eq(uid))
               .fetchOneInto(boolean.class);
    }

    public void deleteUser (int uid) {
       DSLContext dsl = DSL.using(dbConnectionUrl);

       dsl.deleteFrom(userTable)
               .where(userTable.USERID.eq(uid))
               .execute();
    }

    public List<String> getUserByRegtype (String regtype) {
       DSLContext dsl = DSL.using(dbConnectionUrl);

       return dsl.select(userTable.USERTAG)
               .from(userTable)
               .where (userTable.REGTYPE.eq(regtype))
               .fetchInto(String.class);
    }

    public List<String> getBadgeOrders () {
       DSLContext dsl = DSL.using(dbConnectionUrl);

       return dsl.select(userTable.USERTAG)
               .from(userTable)
               .where(userTable.BADGE.eq(true))
               .fetchInto(String.class);
    }

    public List<Integer> getRegistrationCount() {
       DSLContext dsl = DSL.using(dbConnectionUrl);

       return dsl.select(userTable.ID)
               .from(userTable)
               .fetchInto(Integer.class);
    }

}
