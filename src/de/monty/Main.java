package de.monty;

import de.monty.Methods.Database;
import de.monty.Bots.FurwalkBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class Main {
    /* 	Offene Punkte
     * 	TODO Manuelles eintragen / austragen von Teilnehmern durch Admins
     *	TODO Eintragen von Badgenamen - Zusätzliche Abfrage
     *	TODO Infopush an vorherige Teilnehmer um zur nächsten Veranstaltung einzuladen - Manuell auslösbare Admin-Funktion
     *	TODO Bearbeitung der strings über Interaktion mit dem Bot - laden aus DB
     *	TODO Überarbeiten der Teilnehmeranzeige und Datenspeicherung. @Namen sind nicht zuverlässig und ändern sich. Nach Möglichkeit zur Laufzeit abfragen oder durch regelmäßig stattfindenden Task auffrischen lassen.
     *	TODO Streamlining der User Interaktionen
     *	TODO Vernünftiges Logging & Comments
     */

    static {
        // Load JDBC-Driver
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private static void setupDb() {
        Database db = new Database();

        if (db.noUserExists()) {
            db.insertUser("EricCoon", 119236740, "", false,  true);
            db.insertUser("Exceedingly_Gay_Otter", 673791170, "", false,  true);
        }
    }

    public static void main(String[] args) {
        // DB vorbereiten
        setupDb();

        // Funktion?
        ApiContextInitializer.init();

        TelegramBotsApi botsapi = new TelegramBotsApi();

        try {
            // Verknüpfen des Bots
            botsapi.registerBot(new FurwalkBot());
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        //Wird diese Line erreicht? Müsste er nicht in der vorherigen Line stecken und da loopen? Oder wo ist der main loop?
        System.out.println("Furwalkbot active!");
    }
}
