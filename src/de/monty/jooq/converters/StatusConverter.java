package de.monty.jooq.converters;

import de.monty.Status;
import org.jooq.Converter;

public final class StatusConverter implements Converter<String, Status> {

    @Override
    public Status from(String databaseObject) {
        if (databaseObject == null) {
            return null;
        }
        return Status.valueOf(databaseObject);
    }

    @Override
    public String to(Status userObject) {
        if (userObject == null) {
            return null;
        }
        return userObject.name();
    }

    @Override
    public Class<String> fromType() {
        return String.class;
    }

    @Override
    public Class<Status> toType() {
        return Status.class;
    }
}
