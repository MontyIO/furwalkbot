package de.monty.Bots;


import de.monty.Status;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Update;
import de.monty.Methods.*;
import de.monty.jooq.codegen.tables.records.UserRecord;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class FurwalkBot extends TelegramLongPollingBot {
//setup
    @Override
    public String getBotToken() {
        return "815300438:AAFjKyMroeXpIQObw4hwWH1cGv61imIGxoI";
    }

    @Override
    public String getBotUsername() {
        return "MontysFurwalkBot";
    }
//setup ende

    @Override
    public void onUpdateReceived(Update update) {
        // Variablen setup
        Database db = new Database();

        long cid; //ChatId
        int mid; //MessageId
        int uid; //User id
        String utg; //Usertag TODO ist das Displayname oder @Name?
        String input = ""; //Message text TODO Was soll das heißen?
        Status currentStatus;
        String callData = ""; // TODO OwO Whats dis?
        boolean isAdmin;
        boolean isRegistered;
        /* Fragen:
         * wir reagieren hier auf die Nachricht die reinkommt.
         * Wie wird bspw. /menu dargestellt?
         * Noch mehr fragen am ausdenken XD
         */

        if (update.hasCallbackQuery()) {
            //set variables in case of inline button event
            callData = update.getCallbackQuery().getData();
            utg = update.getCallbackQuery().getFrom().getUserName();
            uid = update.getCallbackQuery().getFrom().getId();
            cid = update.getCallbackQuery().getMessage().getChatId();
            mid = update.getCallbackQuery().getMessage().getMessageId();
            currentStatus = db.getStatus(uid);
        }
        else {
            //Set variables in case of a Message update
            utg = update.getMessage().getFrom().getUserName();
            uid = update.getMessage().getFrom().getId();
            cid = update.getMessage().getChatId();
            mid = update.getMessage().getMessageId();
            input = update.getMessage().getText();
            currentStatus = db.getStatus(uid);
        }

        /*
         * Admin Check
         */

        UserRecord admin = db.getAdminStatus(uid);
        isAdmin = admin !=null;

        /*
         * registration check
         */

        UserRecord user = db.getUser(uid);
        isRegistered = user !=null;


        /*
         * Call start menu for normal user
         */

        if (input.equals("/start") && !isRegistered || input.equals("/menu") && !isRegistered) {

            SendMessage menu = new Menu()
                    .unregStart(cid);

            try {
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            return;
        }

        if (input.equalsIgnoreCase("/menu") && isRegistered || input.equalsIgnoreCase("/start") && isRegistered) {

            SendMessage menu = new Menu()
                    .regStart(uid, cid);

            try {
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        /*
         * Call admin menu
         */

        if (input.equalsIgnoreCase("/admin") && isAdmin) {

            SendMessage menu = new Menu()
                    .adminStart(cid, utg);

            try {
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        /*
         * Registration
         */

        if (callData.equals("register")) {

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage menu = new Menu()
                    .regtypeMenu(cid);

            try {
                execute(dmessage);
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
            db.insertUser(utg, uid, null, false, false);
            db.updateStatus(Status.BADGE, uid);
            return;
        }

       if (currentStatus.equals(Status.BADGE)) {
            db.updateRegtype(callData, uid);

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

           SendMessage menu = new Menu()
                   .badgeMenu(cid);

            try {
                execute(dmessage);
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
            db.updateStatus(Status.BADGE_2, uid);

        } else if (currentStatus.equals(Status.BADGE_2)) {

           switch (callData) {
               case "badge":
                   db.updateBadge(uid, true);
                   break;
               case "!badge":
                   db.updateBadge(uid, false);
                   break;
           }

           DeleteMessage dmessage = new DeleteMessage()
                   .setChatId(cid)
                   .setMessageId(mid);

           SendMessage message = new SendMessage()
                   .setChatId(cid)
                   .setText("Du bist erfolgreich angemeldet! Wir freuen uns auf dich!" +"\n\n"
                   +"Drück bitte auf /menu um deinen Status anzuzeigen oder weitere Änderungen durchzuführen.");

           try {
               execute(dmessage);
               execute(message);
           } catch (TelegramApiException e) {
               //ignored
           }

           db.updateStatus(Status.NONE, uid);

       }

        /*
         * Edit entry data
         */

        //Regtype
        if (callData.equals("editRegtype")) {

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage menu = new Menu()
                    .regtypeMenu(cid);

            try {
                execute(dmessage);
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
            db.updateStatus(Status.EDIT_REGTYPE, uid);
        } else if (currentStatus.equals(Status.EDIT_REGTYPE)) {
            db.updateRegtype(callData, uid);

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage message = new SendMessage()
                    .setChatId(cid)
                    .setText("Die Änderung wurde gespeichert." +"\n\n"
                            +"Drück bitte auf /menu um deinen Status anzuzeigen oder weitere Änderungen durchzuführen.");

            try {
                execute(dmessage);
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            db.updateStatus(Status.NONE, uid);
        }

        //Badge
       if (callData.equals("editBadge")) {

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage menu = new Menu()
                    .badgeMenu(cid);

            try {
                execute(dmessage);
                sendApiMethod(menu);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }

            db.updateStatus(Status.EDIT_BADGE, uid);
        } else if (currentStatus.equals(Status.EDIT_BADGE)) {

            switch (callData) {
                case "badge":
                    db.updateBadge(uid, true);
                    break;
                case "!badge":
                    db.updateBadge(uid, false);
                    break;
            }

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage message = new SendMessage()
                    .setChatId(cid)
                    .setText("Ihre anmeldedaten wurden bearbeitet, sie können mit /menu zurückkehren" +
                            " und sie überprüfen oder nochmals ändern.");

            try {
                execute(dmessage);
                execute(message);
            } catch (TelegramApiException e) {
                //ignored
            }

            db.updateStatus(Status.NONE, uid);

        }

        /*
         * deregister
         */

        if (callData.equals("deregister")) {

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage message = new SendMessage()
                    .setChatId(cid)
                    .setText("Du bist jetzt abgemeldet. Wir hoffen dich beim nächsten Walk wieder begrüßen zu dürfen!");

            db.deleteUser(uid);

            try {
                execute(dmessage);
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        /*
         * Admin Tools
         */

        //Registration list

        if (callData.equals("Reglist")) {

            TextFactory tf = new TextFactory();

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage message = new SendMessage()
                    .setChatId(cid)
                    .setText(tf.buildRegtypeUserList());

            try {
                execute(dmessage);
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }

        //Badge list
        if (callData.equals("Badgelist")) {

            TextFactory tf = new TextFactory();

            DeleteMessage dmessage = new DeleteMessage()
                    .setChatId(cid)
                    .setMessageId(mid);

            SendMessage message = new SendMessage()
                    .setChatId(cid)
                    .setText(tf.buildBadgeList());

            try {
                execute(dmessage);
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }
}

